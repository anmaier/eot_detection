Paper source and python code for running Interspeech 2017 submission experiments on end-of-turn detection. Please cite the following paper if you use this repository:

```
@inproceedings{MaierEtAl17InterSpeech,
  author       = {Maier, Angelika and Hough, Julian and Schlangen, David},
  booktitle    = {Proceedings of INTERSPEECH 2017},
  location     = {Stockholm, Sweden},
  title        = {{Towards Deep End-of-Turn Prediction for Situated Spoken Dialogue Systems}},
  year         = {2017},
}
```

Details on how to set up the code and reproduce the experiments in the paper can be found in `Code/README.md`.